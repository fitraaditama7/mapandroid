﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Gms.Location.Places.UI;
using Android.Content;
using Android.Runtime;

namespace AndroidMap
{
    [Activity(Label = "AndroidMap", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private static readonly int PLACE_PICKER_REQUEST = 1;

        private Button _pickAPlaceButton;
        private TextView _placeNameTextView;
        private TextView _placeAddressTextView;
        private TextView _placePhoneNumberTextView;
        private TextView _placeWebsiteTextView;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _placeNameTextView = FindViewById<TextView>(Resource.Id.main_placeName);
            _placeAddressTextView = FindViewById<TextView>(Resource.Id.main_placeAddress);
            _placePhoneNumberTextView = FindViewById<TextView>(Resource.Id.main_placePhoneNumber);
            _placeWebsiteTextView = FindViewById<TextView>(Resource.Id.main_placeWebsite);

            _pickAPlaceButton = FindViewById<Button>(Resource.Id.main_pickAPlaceButton);

            _pickAPlaceButton.Click += OnPickAPlaceTapped;
        }

        private void OnPickAPlaceTapped(object sender, EventArgs e)
        {
            var builder = new PlacePicker.IntentBuilder();
            StartActivityForResult(builder.Build(this),1);
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == PLACE_PICKER_REQUEST && resultCode == Result.Ok)
            {
                GetPlaceFromPicker(data);
            }

            base.OnActivityResult(requestCode, resultCode, data);
        }

        private void GetPlaceFromPicker(Intent data)
        {
            var placePicked = PlacePicker.GetPlace(this, data);

            _placeNameTextView.Text = placePicked?.NameFormatted?.ToString();
            _placeAddressTextView.Text = placePicked?.AddressFormatted?.ToString();
            _placePhoneNumberTextView.Text = placePicked?.PhoneNumberFormatted?.ToString();
            _placeWebsiteTextView.Text = placePicked?.WebsiteUri?.ToString();
        }

    }
}

